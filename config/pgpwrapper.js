"use strict"
const OpenPGP = function(){
	const openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp
	openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path
	openpgp.config.aead_protect = true // activate fast AES-GCM mode (not yet OpenPGP standard)
	openpgp.config.aead_mode = openpgp.enums.aead.eax // Default, native
	
	this.generateKeys = async (keyLen, userIds, passphrase)=>{
		let options = {
			userIds: [userIds], // multiple user IDs
			numBits: keyLen, // RSA key size
			passphrase: passphrase // protects the private key
		};

		this.key = await openpgp.generateKey(options);

		this.privateKey = this.key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
		this.publicKey = this.key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
		this.passphrase = passphrase;

		this.privKeyObj = openpgp.key.readArmored(this.privateKey).keys;

		return this.key;
  }
  
  this.generateECCKeys = async (curve, userIds, passphrase)=>{
		let options = {
			userIds: [userIds], // multiple user IDs
			curve: curve, // ECC key curve
			passphrase: passphrase 
		};

		this.key = await openpgp.generateKey(options);
		return this.key;
	}

	this.encrypt = async (data, pubkey, sign)=>{  
		let options = {
      message: openpgp.message.fromText(data), 
			publicKeys: (await openpgp.key.readArmored(pubkey)).keys,  
		};

		if(sign)
		{
			options.privateKeys = this.privKeyObj;
		}

		return (await openpgp.encrypt(options)).data; // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
	}

	this.decryptAndVerifySignature = async (encrypted, publicKey, privateKey)=>{
    let options = {
			message: (await openpgp.message.readArmored(encrypted)),     
			privateKey: (await openpgp.key.readArmored(privateKey)), 
			publicKeys: (await openpgp.key.readArmored(publicKey)).keys
		};
  
    let decrypted = (await openpgp.decrypt(options));

		let valid = true;
		for(let signature of decrypted.signatures)
		{
			if(!signature.valid)
				valid = false;
		}
    
    return {data: decrypted.data, valid: valid}; 
    
	}

	this.decrypt = async (encrypted, privateKey, passphrase)=>{
    const privKeyObj = (await openpgp.key.readArmored(privateKey)).keys[0];
		await privKeyObj.decrypt(passphrase);
    let options = {
			message: (await openpgp.message.readArmored(encrypted)),   
			privateKeys: [privKeyObj]
		};

		return (await openpgp.decrypt(options)).data; 
	}

	this.sign = async (data, privateKey, passphrase)=>{
		const privKeyObj = (await openpgp.key.readArmored(privateKey)).keys[0];
		await privKeyObj.decrypt(passphrase)
		let options = {
      message: openpgp.message.fromText(data), 
			privateKeys: [privKeyObj],
			detached: true
		};
		await openpgp.sign(options).then(function(signed) {
			return signed.signature; // '-----BEGIN PGP SIGNED MESSAGE ... END PGP SIGNATURE-----'
	});
		return (await openpgp.sign(options)).signature; 
	}

	this.verify = async (data, detachedSig, pubkey)=>{

		let options = {
			message: await openpgp.message.fromText(data), 
			signature: await openpgp.signature.readArmored(detachedSig), 
			publicKeys: (await openpgp.key.readArmored(pubkey)).keys
		};
		var validity;
		openpgp.verify(options).then(function(verified) {
    validity = verified.signatures[0].valid; // true
    if (validity) {
			return verified.signatures[0].keyid.toHex();
		}
		else return 'Not valid signature';
	})
		return (await openpgp.verify(options)).signatures[0].keyid.toHex(); // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
	}
};

module.exports = OpenPGP;