const express = require('express');
"use strict"
const bodyParser = require('body-parser');
var compression = require('compression');
const router = express.Router();
const axios = require('axios');
const OpenPGP = require('./config/pgpwrapper.js');
var passport = require('passport');
require('dotenv').config();
var User = require('./models/User');
var UserSession = require('./models/UserSession');

var users = require('./routes/api/users');
var keys = require('./routes/api/keys');
const path = require('path'); //AWS
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const mongoose = require('mongoose')
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(db,{ useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

app.use('/api/users', users);
app.use('/api/keys', keys);

// Server static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));
  app.use('/static', express.static(path.join(__dirname, 'client/build')));
}
app.use(compression())

const port = process.env.PORT// || 3000;
app.listen(process.env.PORT || port)

module.exports = router;
