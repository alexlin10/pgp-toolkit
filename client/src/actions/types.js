export const GET_ERRORS = 'GET_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_KEY = 'SET_KEY';
export const GET_KEYS = 'GET_KEYS';
export const KEYS_LOADING = 'KEYS_LOADING';
export const CLEAR_CURRENT_KEYS = 'CLEAR_CURRENT_KEYS';
export const SET_PASS = 'SET_PASS';