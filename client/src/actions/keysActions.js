import axios from 'axios';
import { GET_KEYS, KEYS_LOADING, CLEAR_CURRENT_KEYS, GET_ERRORS, SET_KEY, SET_PASS } from './types';

//Get current keys
export const getCurrentKeys = (userid) => dispatch => {
  dispatch(setKeyLoading());
  axios.get(`/api/keys/${userid}`)
    .then(res => dispatch({
      type: GET_KEYS,
      payload: res.data
    })
  )
  .catch(err => dispatch({
    type: GET_KEYS,
    payload: {}
  })
)}

//Set selected key for crypto functions
export const setKey = (id) => dispatch => {
  dispatch({
    type: SET_KEY,
    payload: id
  })
}
//Set passphrase
export const setPass = (passphrase) => dispatch => {
  dispatch ({
    type: SET_PASS,
    payload: passphrase
  })
}

//Generate Keys
export const generateKeys = (keyData, history) => dispatch => {
  axios
    .post('/api/keys/generate', keyData)
    .then(res => history.push('/keys'))
    .catch(err => 
      dispatch({
          type: GET_ERRORS,
          payload: err.response.data
      }));
}

export const deleteKey = (_id) => dispatch => {
  if (window.confirm('Please confirm below to delete the key')) {
    axios
      .post(`/api/keys/delete/${_id}`)
      .then(res => window.location.reload())
      .catch(err => 
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
  }
}

//Keys loading
export const setKeyLoading = () => {
  return {
    type: KEYS_LOADING
  }
}


//Clear keys
export const clearCurrentKeys = () => {
  return {
    type: CLEAR_CURRENT_KEYS
  }
}

