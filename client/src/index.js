import React from 'react'; 
import ReactDOM from 'react-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {  Container } from 'reactstrap';
import registerServiceWorker from './registerServiceWorker';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';

import Dashboard from './components/dashboard/Dashboard';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import jwt_decode from 'jwt-decode';
import setAuthToken from './actions/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';
import PrivateRoute from './components/common/PrivateRoute';

import Keys from './components/keyManagement/Keys';
import GenerateKeys from './components/keyManagement/GenerateKeys';


registerServiceWorker();

//Check for token
if(localStorage.jwtToken){
    //Set auth token header auth
    setAuthToken(localStorage.jwtToken);
    //Decode token and get user info
    const decoded = jwt_decode(localStorage.jwtToken);
    //Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));

    //Check for expired
    const currentTime = Date.now() / 1000;
    if(decoded.exp < currentTime){
      //Logout
      store.dispatch(logoutUser());
      
      //Redirect to login
      window.location.href = '/login';
    }
}


class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
      <Router>
       <div className="App">
        <Navbar />
        <Route exact path="/" component={Login} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Switch>
         <PrivateRoute exact path="/dashboard" component={Dashboard} />
        </Switch>
        <Switch>
          <PrivateRoute
            exact path="/keys" component={Keys} />
        </Switch>
        <Switch>
          <PrivateRoute
            exact path="/generatekeys" component={GenerateKeys} />
        </Switch>
        <Container>
        <br></br>
          <Footer />
          </Container>
      </div>
     </Router>
    </Provider>
    );
  }
};
const app = document.getElementById('root');
ReactDOM.render(<App />, app);