import { SET_KEY, GET_KEYS, KEYS_LOADING, CLEAR_CURRENT_KEYS, SET_PASS } from '../actions/types';

const initialState = {
  keys: [],
  selected: [],
  passphrase: [],
  loading: false
}

export default function(state = initialState, action){
  switch(action.type){
    case KEYS_LOADING:
      return {
        ...state,
        loading: true
      }
      case SET_KEY:
      return{
        ...state,
        selected: action.payload
      }
      case GET_KEYS:
      return {
        ...state,
        keys: action.payload,
        loading: false
      }
      case SET_PASS:
      return {
        ...state,
        passphrase: action.payload
      }
      case CLEAR_CURRENT_KEYS:
      return {
        ...state,
        passphrase: null,
        keys: null,
        selected: null
      }
    default:
      return state;
  }
}