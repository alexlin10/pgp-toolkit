import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import keyReducer from './keyReducer';

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  pgpkeys: keyReducer
});