import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { getCurrentKeys } from '../../actions/keysActions';
import { withRouter } from 'react-router-dom';
import Modal from 'react-awesome-modal';
import { Button } from 'reactstrap';
import { deleteKey } from '../../actions/keysActions';
import { Link } from 'react-router-dom';
import DownloadLink from "react-download-link";
class Keys extends Component {
  constructor(props) {
    super(props);
    this.state = {
        keyType: "",
        keyData: "",
        visible : false
    }
    this.style = {
      textAlign: "left",
      paddingLeft: "80px"
    }
    this.buttonStyle = {
      paddingBottom: "5 px"
    }
  }
  openModal(keyType, keyData) {
    this.setState({
        keyType: keyType,
        keyData: keyData,
        visible : true
    });
  }

  closeModal() {
      this.setState({
         visible : false
     });
  }

  componentDidMount(){
    const { user } = this.props.auth;
    this.props.getCurrentKeys(user.id);
  }

  onDeleteClick(keyid){
    this.props.deleteKey(keyid);
  }




  render() {
    const { keys, loading } = this.props.pgpkeys;
    //const { user } = this.props.auth;
    let keysContent = '';

    if(keys === null || loading || keys === undefined) {
      keysContent = <h4>Loading keys...</h4>
    } else   //Keys detected, do nothing
       if(Object.keys(keys).length > 0)       
        keysContent = '';
      
     else {
      keysContent = (
        <div><p>You have not setup keys yet, please generate one by clicking below. </p>
        <Link to="/generatekeys" className="btn btn-lg btn-info">Generate Key</Link>
        <br /> <br /></div>
        );
      }
    return (
      
      <div className="keys">
        <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h1 className="display-4">PGP Key Management</h1>
                <br /> {keysContent}
                {keys.map(keys => 
                   <tr key={keys._id}> 
                   <td style={this.buttonStyle} className="col-12">  <h1>{keys.nameOfKey} </h1>  <p> <b>Fingerprint:</b> {keys.fingerprint} <br />
                   <b>Key id:</b>{keys.keyId}  <br /> <b> Created By: </b>{keys.name} <br /> <b> Created At: </b>{keys.createdAt.substr(0,10)}</p></td>
                   <td><Button size="100px" color="secondary" value="Open" onClick={() => this.openModal('Public Key', keys.pubkey)} >  Public Key </Button> 
                   <Button  size="100px" color="secondary" value="Open" onClick={() => this.openModal('Private Key', keys.privkey)} >Private Key</Button> 
                   <Button  size="100px" color="danger" value="Open" onClick={this.onDeleteClick.bind(this, keys._id)}> Delete Key </Button>
                   </td> 
                </tr>
                   )}
                   
                   <Modal visible={this.state.visible} width="650" height="750" effect="fadeInUp" onClickAway={() => this.closeModal()}> 
                    <div>
                        <h1 text>{this.state.keyType}</h1>
                        <pre style={this.style}>{this.state.keyData}</pre>
                  <Button   
                  color="info">
                   <DownloadLink
                          label="Download"
	                        filename="pgpkey.asc"
                        	exportFile={() => Promise.resolve(this.state.keyData)}
                    >
                   </DownloadLink>
                   </Button>
                        <Button onClick={() => this.closeModal()}>Close</Button>
                    </div>
                  </Modal> 
            </div>
          </div>
      </div>
    </div> 
    )
  }
}

Keys.propTypes = {
  getCurrentKeys: PropTypes.func.isRequired,
  deleteKey: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  pgpkeys: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  pgpkeys: state.pgpkeys,
  auth: state.auth
});

export default connect(mapStateToProps, { getCurrentKeys, deleteKey })(withRouter(Keys));