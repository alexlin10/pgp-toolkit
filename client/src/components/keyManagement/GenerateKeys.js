import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup';
import { generateKeys } from '../../actions/keysActions';
import { Button, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { SHA3 } from 'sha3';
class GenerateKeys extends Component {
  constructor(props) {
      super(props);
      this.state = {
        name: '',
        email: '',
        nameOfKey: '',
        password: '',
        dropdownOpen: false,
        dropDownValue: 'Select Key Type',
      };
      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
      this.toggle = this.toggle.bind(this);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.errors){
      this.setState({errors: nextProps.errors});
    }
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }


  onSubmit(e){
    e.preventDefault();
    let passphrase = new SHA3(512);
    passphrase.update(this.state.password);
    passphrase.digest('hex');
    const { user } = this.props.auth;
    const keyData = {
      name: this.state.name,
      email: this.state.email,
      nameOfKey: this.state.nameOfKey,
      password: passphrase.digest('hex'),
      userId: user.id
    }
    this.props.generateKeys(keyData, this.props.history);
  }

  onChange(event){
    this.setState({[event.target.name]: event.target.value });
  }

  render() {
    //const {errors} = this.state;
    
    return (
      <div className="generate-key">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
            <h1 className="text-center">Generate your PGP key</h1>
            <form onSubmit={this.onSubmit}>
            <TextFieldGroup
                placeholder = "Name of Key"
                name="nameOfKey"
                value={this.state.nameOfKey}
                onChange={this.onChange}
                //errors={errors.handle}
                />
            <TextFieldGroup
                placeholder = "Email"
                name="email"
                value={this.state.email}
                onChange={this.onChange}
                //errors={errors.handle}
                />
              <TextFieldGroup
                placeholder = "Name"
                name="name"
                value={this.state.name}
                onChange={this.onChange}
                //errors={errors.handle}
                />
              <TextFieldGroup
                placeholder = "Password"
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.onChange}
                info="*Password is hashed with SHA3 512 bits before generating key."
                //errors={errors.handle}
                />
                <br />
                <Button type="submit" color="secondary" className="btn-block" >Generate Key</Button>
            </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

GenerateKeys.propTypes = {
  generateKeys: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  pgpkeys: state.keys,
  errors: state.errors,
  auth: state.auth
})

export default connect(mapStateToProps, {generateKeys})((GenerateKeys));