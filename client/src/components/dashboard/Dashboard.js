import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentKeys, setKey, setPass } from '../../actions/keysActions';

import  EncryptForm  from './Encrypt'
import  DecryptForm  from './Decrypt'
import  SignForm  from './Sign'
import  VerifyForm  from './Verify'

import { Link } from 'react-router-dom';
import { Row, Col, Input, FormGroup, Label, ButtonDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap';
import { SHA3 } from 'sha3';


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropDownValue: 'Select PGP Key',
      pgppass: '',
      dropdownOpen: false,
      visible : false
    };
    this.toggle = this.toggle.bind(this);
    this.changeValue = this.changeValue.bind(this);
    this.onChange = this.onChange.bind(this);
  }

toggle(event) {
    this.setState({
        dropdownOpen: !this.state.dropdownOpen
    });
}

changeValue(event) {
    this.setState({dropDownValue: event.currentTarget.textContent + ' selected'});
    let id = event.currentTarget.getAttribute("id");
    let { keys } = this.props.pgpkeys;
    let selectedkey = keys.find(obj => {
      return obj._id === id;
    });
    this.props.setKey(selectedkey);
}

onChange(event){
  let passphrase = new SHA3(512);
  passphrase.update(event.target.value);
  this.props.setPass(passphrase.digest('hex'));
}

  componentDidMount(){
    const { user } = this.props.auth;
    this.props.getCurrentKeys(user.id);
    let passphrase = new SHA3(512);
    passphrase.update("");
    this.props.setPass(passphrase.digest('hex'));
  }
  
  
  render() {
    const { keys, loading } = this.props.pgpkeys;
    let keysContent;
    let passform;
    let keyNames = [];
    if (keys){
      for (let values of keys.values()){  
      keyNames.push(values);
      }
    }
    const ListItem = (key) => {
      return <DropdownItem id={key._id} onClick={this.changeValue} >{ key.nameOfKey }</DropdownItem >;
    }
    
    if(keys === null || loading || keys === undefined) {
      keysContent = <h4>Loading keys...</h4>
    } else { 
      if(Object.keys(keys).length > 0) {       //Display generate button if no keys detected.  Later use logic if private key is not detected to only show Encrypt/Verify functions
        keysContent = 
          <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
          <DropdownToggle caret>
            {this.state.dropDownValue}
          </DropdownToggle>
          <DropdownMenu> 
            { keyNames.map( (item, i) => <ListItem key = {item._id} _id = {item._id} nameOfKey={ item.nameOfKey } /> )}
          </DropdownMenu>
          </ButtonDropdown>

        passform = 
          <Row> <Col sm="12" md={{ size: 2, offset: 5 }}>
          <FormGroup>
          <Label for="pgppass" hidden>Password</Label>
          <Input type="password" name="password" onChange = {this.onChange} id={this.state.pgppass} placeholder="PGP Password" />
          </FormGroup></Col></Row>;
    } else {
      keysContent = (
        <div><p>You have not setup keys yet, please generate one by clicking below. </p>
        <Link to="/generatekeys" className="btn btn-lg btn-info">Generate Key</Link>
        <br /></div>
        );
      }
    }

    return (

      <div className="dashboard">
        <div className="container">
            <div className="row">
              <div className="col-md-12">
                {keysContent}  <br /> <br />  {passform} 
                <br /><br /> </div> 
                <div className="col-md-6">
                <EncryptForm /></div>
                <br />  <div className="col-md-6">
                <DecryptForm /> 
                < br /> </div> <div className="col-md-6">
                <SignForm /></div>
                <div className="col-md-6">
                <VerifyForm /></div>
       </div>
      </div> 
    </div>
    )
  }
}



Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
  pgpkeys: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  pgpkeys: state.pgpkeys
});

export default connect(mapStateToProps, { getCurrentKeys, setKey, setPass })(Dashboard);
