import React from 'react';
import axios from 'axios';
import { Button, Container, Input } from 'reactstrap';
import Modal from 'react-awesome-modal';

//Add Redux
import PropTypes from 'prop-types';
import {connect} from 'react-redux';



class EncryptForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      encrypted: '',
      visible : false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  openModal() {
    this.setState({
        visible : true
    });
  }

  closeModal() {
      this.setState({
         visible : false,
         encrypted: ''
     });
  }

  handleChange(event) {
    this.setState({
      input: event.target.value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    const submit = { plaintext: this.state.input, _id: this.props.pgpkeys.selected._id };
    this.setState({
      input: ''
    });

    axios.post('/api/keys/encrypt', { submit })
    .then(res => {
      this.setState({
        encrypted: res.data.encrypted
      });
    })
  
    this.style = {
      textAlign: "left",
      paddingLeft: "40px"
    }
}
  render() {
    const { keys, loading } = this.props.pgpkeys;
    return (
      <div>
        <Container>
        <h5>Enter text to encrypt</h5>
        <form onSubmit={this.handleSubmit}>
          <Input type="textarea" value={this.state.input} onChange={this.handleChange}/> <br></br>
           <Button color="secondary" type='submit' value="Open" onClick={() => this.openModal(keys.pubkey)} > Encrypt </Button>
              <Modal visible={this.state.visible} width="600" height="450" effect="fadeInUp" onClickAway={() => this.closeModal()}>
              <div>
                  <h1>Encrypted message</h1>
                  <pre style={this.style}>{this.state.encrypted}</pre>
                  <Button onClick={() => this.closeModal()}>Close</Button>
              </div>
            </Modal> 
            </form>
          </Container>
      </div>
    );
  }
};

EncryptForm.propTypes = {
   auth: PropTypes.object.isRequired,
   pgpkeys: PropTypes.object.isRequired
 }
 
 const mapStateToProps = state => ({
   pgpkeys: state.pgpkeys,
   auth: state.auth
 });
 
 export default connect(mapStateToProps)(EncryptForm);
