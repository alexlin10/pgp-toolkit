import React from 'react'

export default function () {
  return (

    <footer className="footer.bg-doNotTrack.text-white.mt-5.p-4.text-Center">
    Copyright &copy; {new Date().getFullYear()} PGP toolset
    </footer>
  )
}
