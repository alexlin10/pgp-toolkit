


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const keySchema = new Schema({
  name: String, 
  nameOfKey: String,
  userId: String,
  fingerprint: String,
  keyId: String,
  email: String,
  privkey: String, // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
  pubkey: String,   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
  revocationSignature: String // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
  
 }, {timestamps:true});

 const pgpKey = mongoose.model('Keys', keySchema);
 module.exports = pgpKey;