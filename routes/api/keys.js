const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs'); 
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Load Input Validation
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');
const OpenPGP = require('../../config/pgpwrapper.js');
const bodyParser = require('body-parser');

// Load Key model
var pgpKey = require('../../models/Keys');

// @route   GET api/keys/
// @desc    Gets all keys
// @access  Private

router.get('/:userId', passport.authenticate('jwt', { session: false }), 
(req, res) => {
  const errors = {};
  const userId = req.params.userId;
  pgpKey.find({userId: userId})
    .then(keys => {
      if (!keys) {
        //errors.noprofile = 'There are no keys';
        return res.status(404).json(errors);
      }
      res.json(keys);
    })
    .catch(err => res.status(404).json({ keys: 'There are no keys' }));
});

// @route   POST api/keys/generate
// @desc    Generate Keys
// @access  Private

router.post('/generate', passport.authenticate('jwt', { session: false }),  
(req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const name = req.body.name;
  const nameOfKey = req.body.nameOfKey;
  const userId = req.body.userId;
  pgpKey.findOne({userId: userId}, (err, data) => { 
    const name = req.body.name;
    (async() => {
      let pgp = new OpenPGP();
      let keys = await pgp.generateECCKeys("p521", { name: name, email: email }, password);
      var getFingerprint = function (key) {
        return key.primaryKey.getFingerprint(key);
      };
      
      var getKeyId = function (key) {
        return key.primaryKey.getKeyId().toHex();
      }
      var keyData = new pgpKey ({
        name: name,
        email: email,
        nameOfKey: nameOfKey,
        userId: userId,
        fingerprint: getFingerprint(keys.key),
        keyId: getKeyId(keys.key),
        pubkey: keys.publicKeyArmored,
        privkey: keys.privateKeyArmored,
        revocationSignature: keys.revocationCertificate
      });
      keyData.save(err=>{
        if(err) console.log(err);
      });
      res.send(keyData);
    })();
  })
});

// @route   POST api/keys/delete/:id
// @desc    Delete all key data
// @access  Private

router.post('/delete/:_id', passport.authenticate('jwt', { session: false }),  
(req, res) => {
  const _id = req.params._id;
  pgpKey.findOneAndDelete({_id: _id}).then(() =>
        res.json({ success: true })
    );
  });

router.post('/delete/all/:userid', passport.authenticate('jwt', { session: false }),  
(req, res) => {
  const userid = req.params._id;
  pgpKey.deleteMany({userId: userid}).then(() =>
        res.json({ success: true })
    );
  });

// @route   POST api/keys/sign
// @desc    Sign plaintext with detached signature
// @access  Private

  router.post('/sign', passport.authenticate('jwt', { session: false }),  
  (req, res, next) => {
    const ptext = req.body.submit.plaintext;
    const keyid = req.body.submit._id;
    const passphrase = req.body.submit.passphrase;
    if (keyid === undefined){
      return res.send({signed: 'Please select a key'})
    }
    pgpKey.findOne({_id: keyid }, (err, data) => {
      if (err) return res.send('Error reading database');
      const pubkey = data.pubkey;
      const privkey = data.privkey;
  
      (async() => {
        try{
          let pgp = new OpenPGP();
          let signed = await pgp.sign(ptext, privkey, passphrase);
          const signheader = '----BEGIN PGP SIGNED MESSAGE-----\nHash: SHA256\n\n';
          res.send({signed: signheader + req.body.submit.plaintext + '\r\n' + signed});
        } catch(err) {
          res.send({signed:"Error: Incorrect passphrase used."})
        }
      })();
    })
  });

// @route   POST api/keys/verify
// @desc    Verify message with detached signature
// @access  Private
  
  router.post('/verify', passport.authenticate('jwt', { session: false }),  
  (req, res, next) => {
    let signature = req.body.submit.signature; 
    signature = signature.substr(signature.indexOf('\n')+1);
    const ptext = req.body.submit.signature.split('-')[1];
    const _id = req.body.submit._id;
    if (_id === undefined){
      return res.send({signed: 'Please select a key'})
    }
    pgpKey.findOne({_id: _id }, (err, data) => {
      if (err) return res.send('Error reading database');
      const pubkey = data.pubkey;
      const checkid = data.keyId;
      (async() => {
        try{
          let pgp = new OpenPGP();
          let verified = await pgp.verify(ptext, signature, pubkey); //Returns keyId
          const signheader = '----BEGIN PGP SIGNED MESSAGE-----\n';
          if (verified === checkid) {
          res.send({signed: 'Verified with PGP key id: ' + verified + '\r\n\n' + signheader + ptext + signature });
          }
          else res.send({signed: 'Incorrect public key used to verify signature.' + '\r\n\n' + signheader + ptext + signature }) //Verify errors later
        } catch(err) {
          console.log(err);
          res.send({signed: 'Error: Something went wrong with the public key or signature.' + '\r\n\n' + signheader + ptext + signature })
        }
      })();
    })
  });
  
// @route   POST api/keys/decrypt
// @desc    Decrypt message
// @access  Private

  router.post('/decrypt', passport.authenticate('jwt', { session: false }),  
  (req, res, next) => {
    const ctext = req.body.submit.ctext;
    const keyid = req.body.submit._id;
    const passphrase = req.body.submit.passphrase;
    if (keyid === undefined){
      return res.send({decrypted: 'Please select a key'})
    }
    pgpKey.findOne({_id: keyid }, (err, data) => {
      if (err) return res.send('Error reading database');
      const pubkey = data.pubkey;
      const privkey = data.privkey;
  
      (async() => {
        try{
          let pgp = new OpenPGP();
          let decrypted = await pgp.decrypt(ctext, privkey, passphrase);
          res.send({decrypted: decrypted});
        } catch(err) {
          res.send({decrypted: 'Error: Incorrect PGP key or passphrase used to decrypt message.'})
        }
      })();
    })
  });

// @route   POST api/keys/encrypt
// @desc    Encrypt plaintext to PGP message
// @access  Private
  
  router.post('/encrypt', passport.authenticate('jwt', { session: false }),  
  (req, res, next) => {
    const ptext = req.body.submit.plaintext;
    const keyid = req.body.submit._id;
    if (keyid === undefined){
      return res.send({encrypted: 'Please select a key'})
    }
    pgpKey.findOne({_id: keyid}, (err, data) => {
      if (err) return res.send('Error reading database');
      const pubkey = data.pubkey;
      (async() => {
        try{
          let pgp = new OpenPGP();
          let encrypted = await pgp.encrypt(ptext, pubkey, false);
          res.send({encrypted: encrypted});
        } catch(err) {
          res.send({encrypted: 'Error: Something went wrong with public key or message. '})
        }
      })();
    })
  });

module.exports = router;
